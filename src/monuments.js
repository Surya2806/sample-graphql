import React, { useEffect, useState } from "react";
import { useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";
import { useHistory } from "react-router-dom";

const GET_MONUMENTS = gql`
  {
    monuments {
      id
      name
    }
  }
`;

const Monuments = () => {
  const history = useHistory();
  const [monuments, setMonuments] = useState([]);
  const { data, loading, error } = useQuery(GET_MONUMENTS);
  useEffect(() => {
    if (loading) return <p>Loading...</p>;
    if (error) return <p>Error</p>;
    else {
      const sortedMonuments = data.monuments.sort((a, b) => {
        return a.id - b.id;
      });
      setMonuments(sortedMonuments);
    }
  }, [data, error, loading]);
  return (
    <div className="w-full max-w-sm mx-auto py-6 rounded shadowed">
      <header className="font-bold text-3xl mb-5 border-bottom">
        Monuments
      </header>
      <ul>
        {monuments?.length &&
          monuments.map((monument) => {
            return (
              <li
                key={monument.id}
                className="cursor-pointer border p-5 rounded hover:shadow"
                onClick={() => {
                  history.push(`/monument/${monument.id}`);
                }}
              >
                {monument.name}              
              </li>
            );
          })}
      </ul>
    </div>
  );
};

export default Monuments;
