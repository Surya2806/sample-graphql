import React, { useEffect, useState } from "react";
import { useQuery } from "@apollo/react-hooks";
import { gql, useMutation } from "@apollo/client";
import { useHistory, useParams } from "react-router-dom";
import { client } from "./index";

const MonumentDetails = () => {
  const history = useHistory();
  const { id } = useParams();
  const GET_MONUMENT_INFO = gql`
    {
      monuments(where: { id: { _eq: ${id} } }) {
        id
        name
        location_name
        status
        view_count
        reviews{
          comment
        }
      }
    }
  `;

  const ADD_REVIEW = gql`
    mutation insert_reviews(
      $comment: String
      $rating: Int
      $user_id: String
      $monument_id: Int
    ) {
      insert_reviews(
        objects: {
          comment: $comment
          rating: $rating
          user_id: $user_id
          monument_id: $monument_id
        }
      ) {
        affected_rows
      }
    }
  `;

  const MONUMENT_INFO_SUBSCRIPTION = gql`
    {
      monuments(where: { id: { _eq: ${id} } }) {
        id
        name
        location_name
        status
        view_count
        reviews{
          comment
        }
      }
    }
  `;

  const [monumentInfo, setMonumentInfo] = useState([]);
  const [review, setReview] = useState("");
  const { data, loading, error } = useQuery(GET_MONUMENT_INFO);
  const [addComment, { options }] = useMutation(ADD_REVIEW);

  useEffect(() => {
    if (loading) return <p>Loading...</p>;
    if (error) return <p>Error</p>;
    else {
      setMonumentInfo(data.monuments[0]);
    }
  }, [data, error, loading, review]);

  const addReviews = (e) => {
    e.preventDefault();
    console.log(review);
    addComment({
      variables: {
        comment: review,
        rating: 10,
        user_id: "Ei15DpJ2qHR35hmYdLTFkt8ngXK2",
        monument_id: id,
      },
    });
    setReview(" ");
  };

  const reviews = monumentInfo.reviews;

  return (
    <div>
      <header className="w-full max-w-sm mx-auto mt-3 px-6 font-bold text-3xl mb-5">
        Monument Details
      </header>
      <div className="w-full max-w-sm mx-auto p-6 border mt-10 rounded shadow bg-gray-100">
        <p className="font-bold text-3xl">{monumentInfo.name}</p>
        <div>
          <p className="font-bold text-xl text-blue-500 mt-3">
            Location - {monumentInfo.location_name}
          </p>
          <p className="text-lg mt-2 text-yellow-700">
            Number of Views - {monumentInfo.view_count}
          </p>
          <p className="text-lg mt-2 text-yellow-700">
            Status - {monumentInfo.view_count ? "Open" : "Closed"}
          </p>
        </div>
        <label className="text-lg mt-2 text-blue-500 font-bold">Reviews</label>
        <ol>
          {reviews?.map((review, index) => {
            return (
              <li key={index} className="ml-5 font-bold mt-3">
                {review.comment}
              </li>
            );
          })}
        </ol>
        <label className="text-lg mt-2 text-yellow-700">Add your Reviews</label>
        <form onSubmit={addReviews}>
          <textarea
            rows={4}
            cols={40}
            value={review}
            className="text-sm bg-gray-300 mt-2 w-100 rounded"
            onChange={(e) => {
              setReview(e.target.value);
            }}
          />
          <button className="ml-64 border px-3 py-1 rounded bg-gray-50 text-sm shadow mt-2 w-max">
            Add Review
          </button>
        </form>
        <button
          className="ml-64 border px-3 py-1 rounded bg-gray-50 text-sm shadow mt-2"
          onClick={() => {
            history.push("/");
          }}
        >
          Back
        </button>
      </div>
    </div>
  );
};

export default MonumentDetails;
