import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Monuments from "./monuments";
import MonumentDetails from "./monument-detail";

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <Monuments />
        </Route>
        <Route exact path="/monument/:id">
          <MonumentDetails />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
