import React from "react";
import ReactDOM from "react-dom";
import "./main.css";
import App from "./App";
import { ApolloClient } from "apollo-client";
import { InMemoryCache } from "apollo-cache-inmemory";
import { ApolloProvider as ApolloHooksProvider } from "@apollo/react-hooks";
import { setContext } from "@apollo/client/link/context";
import { split, HttpLink } from "@apollo/client";
import { getMainDefinition } from "@apollo/client/utilities";
import { WebSocketLink } from "@apollo/client/link/ws";

const link = new HttpLink({
  uri: "https://dev-monuments.nfndev.com/v1/graphql",
});

const wsLink = new WebSocketLink({
  uri: `wss://dev-monuments.nfndev.com/v1/graphql`,
  options: {
    reconnect: true,
  },
});

const authLink = setContext((_, { headers }) => {
  return {
    headers: {
      ...headers,
      "x-hasura-admin-secret": "Remember001",
    },
  };
});

const splitLink = split(
  ({ query }) => {
    const definition = getMainDefinition(query);
    return (
      definition.kind === "OperationDefinition" &&
      definition.operation === "subscription"
    );
  },
  wsLink,
  authLink.concat(link)
);

const cache = new InMemoryCache();

export const client = new ApolloClient({
  cache,
  link: splitLink,
});

ReactDOM.render(
  <React.StrictMode>
    <ApolloHooksProvider client={client}>
      <App />
    </ApolloHooksProvider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
